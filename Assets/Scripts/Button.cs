﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	public GameObject tweenTarget;
	public string overName;
	public Vector3 movePosition;
	public const float DURATION = 0.1f;
	public bool isHover = false;
	private Vector3 initialPosition;

	public bool isOpen;
	// Use this for initialization
	void Start () {
		initialPosition = tweenTarget.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {

		}





	void OnClick(){
		GameObject go = GameObject.Find("Objetos");
		UISprite button_objectos = go.gameObject.GetComponent<UISprite> ();
		button_objectos.spriteName = "btn_objetos";
		
		GameObject go2 = GameObject.Find("Emociones");
		UISprite button_emociones = go2.gameObject.GetComponent<UISprite> ();
		button_emociones.spriteName = "btn_emociones";

		if (isHover) {
						UISprite main_sprite = gameObject.GetComponent<UISprite> ();
						main_sprite.spriteName = overName;
				}
		
		TweenPosition twPosition = TweenPosition.Begin (this.tweenTarget, Button.DURATION, movePosition);
	
		GameObject mContainer = GameObject.Find("MContainer");
		GameObject mContent= GameObject.Find("MContent");

		if (tweenTarget != mContainer) {
			//CIERRA TODOS LOS BLOQUES


				 TweenPosition.Begin (mContainer, Button.DURATION, initialPosition);

			

				}

		if (tweenTarget != mContent) {
			//CIERRA TODOS LOS BLOQUES
			
			
			TweenPosition.Begin (mContent, Button.DURATION, initialPosition);
			
			
			
		}
		}



}
